CREATE DATABASE example DEFAULT CHARACTER SET utf8;
USE example;

CREATE TABLE employee( 
  id varchar (10) PRIMARY KEY
  , name varchar (10) DEFAULT NULL
  , age int
);

insert into employee value ('EMP001', '����^�i', 20);
insert into employee value ('EMP002', '�{�c�\��', 22);

select * from employee;

USE root;
USE textbook_sample;

SELECT * FROM m_customer WHERE cust_id = ?;

SELECT * FROM m_customer;

CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

USE usermanagement;

CREATE TABLE user( 
  id SERIAL PRIMARY KEY
  , login_id varchar(255) UNIQUE
  , name varchar(255) Not Null
  ,birth_date DATE Not Null
  ,password varchar(255) Not Null
  ,create_date DATETIME Not Null
  ,update_date DATETIME Not Null
);


