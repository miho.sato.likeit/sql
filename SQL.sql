﻿create table item_category(category_id primary key auto_increment,category_name varchar(256) not null);
create database 05SQL DEFAULT CHARACTER SET utf8;
use 05sql


create table item_category(category_id int primary key auto_increment,category_name varchar(256) not null);
create table item(item_id int primary key auto_increment,item_name varchar(256) not null,item_price int not null,category_id int);

insert into item_category(category_name) values ('家具');
insert into item_category(category_name) values ('食品');
insert into item_category(category_name) values ('本');

delete from item_category where category_id = 1;
delete from item_category where category_id ='1';
delete from item_category where category_name = '家具';
delete from item_category;
insert into item_category(category_id,category_name) values ('1','家具');
insert into item_category(category_id,category_name) values ('2','食品');
insert into item_category(category_id,category_name) values ('3','本');

insert into item(item_name,item_price,category_id) values ('堅牢な机','3000','1');
insert into item(item_name,item_price,category_id) values ('生焼け肉','50','2');
insert into item(item_name,item_price,category_id) values ('すっきりわかるJava入門','3000','3');
insert into item(item_name,item_price,category_id) values ('おしゃれな椅子','2000','1');
insert into item(item_name,item_price,category_id) values ('こんがり肉','500','2');
insert into item(item_name,item_price,category_id) values ('書き方ドリルSQL','2500','3');
insert into item(item_name,item_price,category_id) values ('ふわふわのベッド','30000','1');
insert into item(item_name,item_price,category_id) values ('ミラノ風ドリア','300','2');

select * from item where item.category_id = '1';
select item_name,item_price from item where item.category_id = '1';

select item_name,item_price from item where item.item_price >= '1000';

select item_name,item_price from item where item_name like '%肉%';

select item_id,item_name,item_price,category_name from item inner join item_category;

USE 05SQL

SELECT 
item_category.category_name,
SUM(item.item_price) AS total_price
FROM 
item 
INNER JOIN 
item_category 
ON
item.category_id = item_category.category_id
GROUP BY item.category_id
ORDER BY total_price DESC;

